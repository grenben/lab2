//
//  Model.h
//  Lab2_Johan_Grenlund
//
//  Created by ITHS on 2016-02-04.
//  Copyright © 2016 johan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

@property (strong, nonatomic) NSString *question;
@property (strong, nonatomic) NSString *answer1;
@property (strong, nonatomic) NSString *answer2;
@property (strong, nonatomic) NSString *answer3;
@property (strong, nonatomic) NSString *answer4;
@property (nonatomic) int rightAnswer;

@end
