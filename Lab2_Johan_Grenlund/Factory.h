//
//  Factory.h
//  Labb2
//
//  Created by Johan Grenlund on 2016-02-05.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"


@interface Factory : NSObject

- (void)creatQuestionArray;

- (Question *)nextQuestion;

- (int)numberOfTimesQuestionIsShowed;

@end
