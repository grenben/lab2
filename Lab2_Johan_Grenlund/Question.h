//
//  Model.h
//  Labb2
//
//  Created by Johan Grenlund on 2016-02-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (strong, nonatomic) NSString *question;
@property (strong, nonatomic) NSString *alt1;
@property (strong, nonatomic) NSString *alt2;
@property (strong, nonatomic) NSString *alt3;
@property (strong, nonatomic) NSString *alt4;
@property (strong, nonatomic) NSString *rightAnswer;
@property (nonatomic) BOOL questionId;

@end
