//
//  Factory.m
//  Labb2
//
//  Created by Johan Grenlund on 2016-02-05.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "Factory.h"
#import "Question.h"

@implementation Factory

NSArray *questionArray;

int _numberOfTimesAnswerIsShown;

- (void)creatQuestionArray {
    
    Question *firstQuestion = [[Question alloc] init];
    firstQuestion.question = @"Vilken reggae stjärna dog 11 Maj 1981?";
    firstQuestion.alt1 = @"Bob Marley";
    firstQuestion.alt2 = @"Daimian Marley";
    firstQuestion.alt3 = @"Kapten Röd";
    firstQuestion.alt4 = @"Tonto Irie";
    firstQuestion.rightAnswer = @"Bob Marley";
    firstQuestion.questionId = NO;
    
    Question *seccondQuestion = [[Question alloc] init];
    seccondQuestion.question = @"I vilken stad början och slutar boken ”Jorden runt på 80 dagar”? ";
    seccondQuestion.alt1 = @"Liverpool";
    seccondQuestion.alt2 = @"Manchester";
    seccondQuestion.alt3 = @"Buckimhamshire";
    seccondQuestion.alt4 = @"London";
    seccondQuestion.rightAnswer = @"London";
    seccondQuestion.questionId = NO;
    
    Question *thirdQuestion = [[Question alloc] init];
    thirdQuestion.question = @"Vilket år dog Karl XII?";
    thirdQuestion.alt1 = @"1718";
    thirdQuestion.alt2 = @"1618";
    thirdQuestion.alt3 = @"1811";
    thirdQuestion.alt4 = @"1758";
    thirdQuestion.rightAnswer = @"1718";
    thirdQuestion.questionId = NO;
    
    Question *fourthQuestion = [[Question alloc] init];
    fourthQuestion.question = @"Annikas förskräckelse – sina pepparkakor?";
    fourthQuestion.alt1 = @"På vinden";
    fourthQuestion.alt2 = @"I sängen";
    fourthQuestion.alt3 = @"På golvet";
    fourthQuestion.alt4 = @"I köket";
    fourthQuestion.rightAnswer = @"På golvet";
    fourthQuestion.questionId = NO;
    
    Question *fifthQuestion = [[Question alloc] init];
    fifthQuestion.question = @"Vad är SAAB en förkortning av?";
    fifthQuestion.alt1 = @"Swedish Air AB";
    fifthQuestion.alt2 = @"Swedish Aeroplan AB";
    fifthQuestion.alt3 = @"Svenska Aeroplan AB";
    fifthQuestion.alt4 = @"Svenska Air AB";
    fifthQuestion.rightAnswer = @"Svenska Aeroplan AB";
    fifthQuestion.questionId = NO;
    
    Question *sixthQuestion = [[Question alloc] init];
    sixthQuestion.question = @"Vilken medelhastighet har ett fordon som färdas 60km på fyra timmar?";
    sixthQuestion.alt1 = @"20km/h";
    sixthQuestion.alt2 = @"15km/h";
    sixthQuestion.alt3 = @"30km/h";
    sixthQuestion.alt4 = @"10km/h";
    sixthQuestion.rightAnswer = @"15km/h";
    sixthQuestion.questionId = NO;
    
    Question *seventhQuestion = [[Question alloc] init];
    seventhQuestion.question = @"Vem var sångaren som sjöng med Mel Gibson i filmen Mad Max: Beyond The Thunderdome?";
    seventhQuestion.alt1 = @"Tina Turner";
    seventhQuestion.alt2 = @"Christina Aquilera";
    seventhQuestion.alt3 = @"Bruce Willis";
    seventhQuestion.alt4 = @"Arnold Schwartzenegger";
    seventhQuestion.rightAnswer = @"Tina Turner";
    seventhQuestion.questionId = NO;
    
    Question *eightQuestion = [[Question alloc] init];
    eightQuestion.question = @"Vodka, Galliano och apelsinjuice är ingedienserna till en klassisk dryck, vilken?";
    eightQuestion.alt1 = @"Harvery Wallbanger";
    eightQuestion.alt2 = @"Cosmopolitan";
    eightQuestion.alt3 = @"Screwdriver";
    eightQuestion.alt4 = @"P2";
    eightQuestion.rightAnswer = @"Harvery Wallbanger";
    eightQuestion.questionId = NO;
    
    Question *ninethQuestion = [[Question alloc] init];
    ninethQuestion.question = @"Vilken amerikansk stat tillhörde förut Sovjetunionen?";
    ninethQuestion.alt1 = @"Hawaii";
    ninethQuestion.alt2 = @"Alaska";
    ninethQuestion.alt3 = @"Idaho";
    ninethQuestion.alt4 = @"Florida";
    ninethQuestion.rightAnswer = @"Alaska";
    ninethQuestion.questionId = NO;
    
    Question *tenthQuestion = [[Question alloc] init];
    tenthQuestion.question = @"Hur många tentakler har en bläckfisk? ";
    tenthQuestion.alt1 = @"Elva";
    tenthQuestion.alt2 = @"Fem";
    tenthQuestion.alt3 = @"Åtta";
    tenthQuestion.alt4 = @"Tio";
    tenthQuestion.rightAnswer = @"Tio";
    tenthQuestion.questionId = NO;

    questionArray = [[NSArray alloc] initWithObjects:firstQuestion,
                     seccondQuestion,
                     thirdQuestion,
                     fourthQuestion,
                     fifthQuestion,
                     sixthQuestion,
                     seventhQuestion,
                     eightQuestion,
                     ninethQuestion,
                     tenthQuestion, nil];

    _numberOfTimesAnswerIsShown = 0;
}

- (int)numberOfTimesQuestionIsShowed {
    return _numberOfTimesAnswerIsShown;
}

- (Question *)nextQuestion {
    Question *currentQuestion = [[Question alloc] init];
    
    for (int i=0; i<questionArray.count; i++) {
        currentQuestion = questionArray[arc4random() % [questionArray count]];
        
        if (currentQuestion.questionId == NO) {
            currentQuestion.questionId = YES;
            
            break;
        }
    }
    _numberOfTimesAnswerIsShown ++;
    
    return currentQuestion;
}

@end
