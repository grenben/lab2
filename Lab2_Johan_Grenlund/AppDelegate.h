//
//  AppDelegate.h
//  Lab2_Johan_Grenlund
//
//  Created by Johan Grenlund on 2016-02-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

