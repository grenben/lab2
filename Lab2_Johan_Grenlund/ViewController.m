//
//  ViewController.m
//  Lab2_Johan_Grenlund
//
//  Created by Johan Grenlund on 2016-02-04.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "ViewController.h"
#import "Factory.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *answer1;
@property (weak, nonatomic) IBOutlet UIButton *answer2;
@property (weak, nonatomic) IBOutlet UIButton *answer3;
@property (weak, nonatomic) IBOutlet UIButton *answer4;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestion;
@property (weak, nonatomic) IBOutlet UIButton *startGamerOver;
@property (weak, nonatomic) IBOutlet UILabel *showAnswerLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreBoardLabel;

@property (strong, nonatomic) Factory *factory;
@property (strong, nonatomic) Question *question;

@property (weak, nonatomic) NSString *answer1Guess;
@property (weak, nonatomic) NSString *answer2Guess;
@property (weak, nonatomic) NSString *answer3Guess;
@property (weak, nonatomic) NSString *answer4Guess;
@property (weak, nonatomic) NSString *rightAnswer;
@property (weak, nonatomic) NSString *questionText;
@end

@implementation ViewController

int numberOfRighGuesses = 0;
int numberOfWrongGuesses = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideButtons];
    self.factory = [[Factory alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)hideButtons {
    self.answer1.hidden = YES;
    self.answer2.hidden = YES;
    self.answer3.hidden = YES;
    self.answer4.hidden = YES;
    self.nextQuestion.hidden = YES;
    self.questionLabel.hidden = YES;
    self.showAnswerLabel.hidden = YES;
    self.scoreBoardLabel.hidden = YES;
}


-(void)showButtons {
    self.answer1.hidden = NO;
    self.answer2.hidden = NO;
    self.answer3.hidden = NO;
    self.answer4.hidden = NO;
    self.nextQuestion.hidden = YES;
    self.startGamerOver.hidden = YES;
    self.questionLabel.hidden = NO;
    self.scoreBoardLabel.hidden = YES;
}

- (void)hideButtonsAndShowAnswerLabel: (NSString *) answer {
    self.answer1.hidden = YES;
    self.answer2.hidden = YES;
    self.answer3.hidden = YES;
    self.answer4.hidden = YES;
    self.showAnswerLabel.hidden = NO;
    NSString *showTextLabel = [NSString stringWithFormat:@"You did answer: \n %@. \n Right answer is: \n %@", answer, self.question.rightAnswer];
    self.showAnswerLabel.text = showTextLabel;
    self.nextQuestion.hidden = NO;
}

- (IBAction)AnswerOneButtonPressed:(UIButton *)sender {
   
    if ([self.question.rightAnswer isEqualToString:self.answer1Guess]) {
        [self hideButtonsAndShowAnswerLabel:self.question.alt1];
        numberOfRighGuesses ++;
        }
    else {
        [self hideButtonsAndShowAnswerLabel:self.question.alt1];
        numberOfWrongGuesses ++;
    }
    
}

- (IBAction)AnswerTwoButtonPressed:(UIButton *)sender {
    if ([self.question.rightAnswer isEqualToString:self.answer2Guess]) {
        [self hideButtonsAndShowAnswerLabel:self.question.alt2];
        numberOfRighGuesses ++;
    }
    else {
        [self hideButtonsAndShowAnswerLabel:self.question.alt2];
        numberOfWrongGuesses ++;
    }
}

- (IBAction)AnswerThreeButtonPressed:(UIButton *)sender {
    if ([self.question.rightAnswer isEqualToString:self.answer3Guess]) {
       [self hideButtonsAndShowAnswerLabel:self.question.alt3];
        numberOfRighGuesses ++;
    }
    else {
        [self hideButtonsAndShowAnswerLabel:self.question.alt3];
        numberOfWrongGuesses ++;
    }
}

- (IBAction)AnswerFoutButtonPressed:(UIButton *)sender {
    if ([self.question.rightAnswer isEqualToString:self.answer4Guess]) {
       [self hideButtonsAndShowAnswerLabel:self.question.alt4];
        numberOfRighGuesses ++;
    }
    else {
       [self hideButtonsAndShowAnswerLabel:self.question.alt4];
        numberOfWrongGuesses ++;
    }
}

- (IBAction)NextQuestionButtonPressed:(UIButton *)sender {
    if ([self.factory numberOfTimesQuestionIsShowed] < 5) {
        [self nextQuestionShow];
        [self populateGui];
        [self showButtons];
        self.nextQuestion.hidden = YES;
        self.showAnswerLabel.hidden = YES;
    }
    else {
        [self hideButtons];
        [self showScoreBoard];
        self.startGamerOver.hidden = NO;
    }
    
}

- (void)showScoreBoard {
    NSString *numberOfWrongGuessesText = [NSString stringWithFormat: @"Number of wrong guesses: \n %d", numberOfWrongGuesses];
    NSString *numberOfGuessesRight = [NSString stringWithFormat:@"Number of right guesses: \n %d", numberOfRighGuesses];
    NSString *scoreText = [NSString stringWithFormat:@"%@\n%@", numberOfGuessesRight, numberOfWrongGuessesText];
    self.scoreBoardLabel.hidden = NO;
    self.scoreBoardLabel.text = scoreText;
    self.startGamerOver.hidden = NO;
}

- (IBAction)StartGameOverButtonPressed:(UIButton *)sender {
    [self showButtons];
    [self.factory creatQuestionArray];
    [self nextQuestionShow];
    [self populateGui];
    numberOfRighGuesses = 0;
    numberOfWrongGuesses = 0;
}

- (void)populateGui {
    self.questionLabel.text = self.questionText;
    [self.answer1 setTitle: self.answer1Guess forState: UIControlStateNormal];
    [self.answer2 setTitle: self.answer2Guess forState: UIControlStateNormal];
    [self.answer3 setTitle:self.answer3Guess forState:UIControlStateNormal];
    [self.answer4 setTitle:self.answer4Guess forState:UIControlStateNormal];
}

- (void)nextQuestionShow {
    self.question = self.factory.nextQuestion;
    self.questionText = self.question.question;
    self.answer1Guess = self.question.alt1;
    self.answer2Guess = self.question.alt2;
    self.answer3Guess = self.question.alt3;
    self.answer4Guess = self.question.alt4;
    self.rightAnswer = self.question.rightAnswer;
}













@end
